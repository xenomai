/**
 * @file
 * Comedi for RTDM, driver facilities
 *
 * Copyright (C) 1997-2000 David A. Schleef <ds@schleef.org>
 * Copyright (C) 2008 Alexis Berlemont <alexis.berlemont@free.fr>
 *
 * Xenomai is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Xenomai is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xenomai; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __COMEDI_COMEDI_DRIVER__
#define __COMEDI_COMEDI_DRIVER__

#ifndef __KERNEL__
#error This header is for kernel space usage only. \
       You are likely looking for <comedi/comedi.h>...
#endif /* !__KERNEL__ */

#include <comedi/os_facilities.h>
#include <comedi/context.h>
#include <comedi/device.h>

#endif /* __COMEDI_COMEDI_DRIVER__ */
